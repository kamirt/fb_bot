$(function () {
    $(".ajax-form").parsley({
        listeners: {
            onFormValidate: function (isFormValid, event, ParsleyForm) {

                if (isFormValid) {
                    ParsleyForm.$element.ajaxSubmit({
                        dataType: "json",
                        success: function (data) {
                            setTimeout(function () {
                                ParsleyForm.$element.find("input[type='text'], textarea").val('');
                            }, 100);
                        }
                    });
                }

                return false;
            }
        }
    });
});