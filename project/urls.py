# -*- coding: utf-8 -*-


from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.utils.encoding import iri_to_uri
from django.conf import settings
from filebrowser.sites import site

from core.views import *


admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^.well-known/acme-challenge/73rvYjrM_s9toSOe_7Sj88he6hT0wOcgptKobgjrv9Y$', AcmeView.as_view()),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin-grappelli/', include('grappelli.urls')),
    url(r'^admin/', include('smuggler.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^fb-bot/', include('fb_bot.urls')),
)

if settings.DEBUG:
    media_url = settings.MEDIA_URL[1:] if settings.MEDIA_URL.startswith('/') \
        else settings.MEDIA_URL

    urlpatterns += patterns(
        '',
        (r'^{0}(?P<path>.*)$'.format(iri_to_uri(media_url)),
         'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}
        ),
    )