from django.conf.urls import patterns, include, url
from django.conf import settings

from fb_bot.views import *


urlpatterns = patterns(
	'',
    url(r'^webhook/$', WebhookView.as_view(), name='webhook'),
)