# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic.base import TemplateView, RedirectView, View
from django.views.generic.detail import DetailView
import random
from django.conf import settings
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
import json
import requests

class WebhookView(View):

    def get(self, request, *args, **kwargs):
        mode = self.request.GET.get('hub.mode', '')
        token = self.request.GET.get('hub.verify_token', '')
        challenge = self.request.GET.get('hub.challenge', '')
        if mode == 'subscribe' and token == settings.FB_PAGE_ACCESS_TOKEN:
            return HttpResponse(challenge, status=200)
        return HttpResponse(status=200)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)


    def post(self, request, *args, **kwargs):

        incoming_message = json.loads(self.request.body.decode('utf-8'))

        for entry in incoming_message['entry']:
            for message in entry['messaging']: 
                if 'message' in message:
                    fbid = message['recipient']['id']
                    received_message = message['message']['text']
                    post_facebook_message(fbid, received_message)    
        return HttpResponse(status=200)



def post_facebook_message(fbid, recevied_message):
    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s' % settings.FB_PAGE_ACCESS_TOKEN
    response_msg = json.dumps({"recipient":{"id":fbid}, "message":{"text":recevied_message}})
    status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)


