# -*- coding: utf-8 -*-
from django.utils import translation
from django.utils.translation import check_for_language
from django.views.generic.base import TemplateView, RedirectView, View
from django.views.generic.detail import DetailView
import random
from django.conf import settings

from core.models import *
from kickass_forms.mixins import RejectStaffUserMixin

from django.http import HttpResponse
__all__ = ['HomeView', 'AcmeView']


class HomeView(RejectStaffUserMixin, TemplateView):
    template_name = 'home.html'

class AcmeView(RejectStaffUserMixin, TemplateView):
    def get(self, request):
    	return HttpResponse('73rvYjrM_s9toSOe_7Sj88he6hT0wOcgptKobgjrv9Y.uQ6v8KVqy7oMHRSlZuiCVh4SDhKZkRI9NwYJa1PrKB0', status=200)