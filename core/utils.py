# -*- coding: utf-8 -*-
from ipware.ip import get_real_ip
import random
import string
from devserver.modules import DevServerModule
from devserver.modules.sql import sqlparse

try:
    from django.db import connections
except ImportError:
    # Django version < 1.2
    from django.db import connection

    connections = {'default': connection}


def str_trunc(str, max_length=50):
    if len(str) > max_length:
        return u'{0}...'.format(str[:max_length])
    return str


def generate_uid():
    chars = string.digits + string.letters

    return ''.join(random.choice(chars) for _ in range(10))


def get_user_uid(request):
    user_uid = get_real_ip(request)

    # uncomment to local work
    # if not user_uid:
    # user_uid = get_ip(request)

    if not user_uid:
        user_uid = request.session.get('user_uid', None)
        if not user_uid:
            user_uid = generate_uid()

            request.session['user_uid'] = user_uid
    return user_uid


class SQLRealTimeModuleEx(DevServerModule):
    """
    Outputs SQL queries as they happen.
    """

    logger_name = 'sql'

    def process_complete(self, request):
        queries = [
            q for alias in connections
            for q in connections[alias].queries
        ]
        for query in queries:
            self.logger.info(
                sqlparse.format(query['sql'], reindent=True, keyword_case='upper'),
                duration=float(query.get('time', 0)) * 1000
            )